//
//  ViewController.swift
//  lettering
//
//  Created by Eugenii Vlasenko on 28.09.17.
//  Copyright © 2017 Eugenii Vlasenko. All rights reserved.
//

import UIKit
import CoreGraphics

class ViewController: UIViewController {
    let curveText = CurveText()
    
    let doubleTapSelectHandler = UITapGestureRecognizer()
    let unfocusRecognizer = UITapGestureRecognizer()
    
    typealias Shadow = (offset: CGPoint, color: UIColor, weight: CGFloat, blur: CGFloat)
    typealias Curve = (CGPoint, CGPoint, CGPoint)
    typealias Settt = (text: String, textColor: UIColor, min: CGFloat, max: CGFloat, pos: CGFloat, shadow: Shadow, curve: Curve)
    var sets = [Settt]()
    var currentSet: Int = 4
    var currentFont: Int = 2
    
    var dots: [UIView] = [UIView]()
    
    let label = UILabel()
    
    func applySettt(settt: Settt) {
        curveText.curvePattern = settt.curve
        curveText.text = settt.text
        curveText.textColor = settt.textColor
        curveText.font = fonts[currentFont]
        curveText.fontSizeMin = settt.min
        curveText.fontSizeMax = settt.max
        curveText.fontSizeMaxPosition = settt.pos
        curveText.shadowBlur = settt.shadow.blur
        curveText.shadowColor = settt.shadow.color
        curveText.shadowOffset = settt.shadow.offset
        curveText.shadowWeight = settt.shadow.weight
    }
    
    let fonts = [
        UIFont(name: "LETTERING-Regular", size: 1)!,
        UIFont(name: "COMIXON", size: 1)!,
        UIFont.systemFont(ofSize: 1),
        UIFont(name: "CMXNLETTERING-Regular", size: 1)!,
    ]
    
    override func viewDidLoad() {
        sets.append((
            text: "npocto text", textColor: UIColor.black, min: 10, max: 10, pos: 0,
            shadow: (offset: CGPoint(x: 0, y: 0), color: UIColor.black, weight: 0, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0.01), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "BANGG", textColor: UIColor.white, min: 20, max: 40, pos: 0.5,
            shadow: (offset: CGPoint(x: 1, y: 0), color: UIColor.black, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0.2), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "yayks", textColor: UIColor.white, min: 20, max: 40, pos: 0.5,
            shadow: (offset: CGPoint(x: 1, y: -2), color: UIColor.black, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: -0.3), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "Psssh", textColor: UIColor.white, min: 20, max: 40, pos: 0.5,
            shadow: (offset: CGPoint(x: 0, y: 2), color: UIColor.black, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0.6), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "Ygdfdsfsdfsdf", textColor: UIColor.white, min: 50, max: 70, pos: 0.5,
            shadow: (offset: CGPoint(x: 0, y: 2), color: UIColor.black, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0.6), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "yayks", textColor: UIColor.white, min: 20, max: 40, pos: 1,
            shadow: (offset: CGPoint(x: 2, y: 0), color: UIColor.black, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: -0.5), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "Psssh", textColor: UIColor.white, min: 20, max: 40, pos: 0,
            shadow: (offset: CGPoint(x: 0, y: -2), color: UIColor.black, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0.6), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "Psssh", textColor: UIColor.yellow, min: 30, max: 30, pos: 0.5,
            shadow: (offset: CGPoint(x: -1, y: -2), color: UIColor.cyan, weight: 3, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: -0.25), CGPoint(x: 1, y: -0.5))
        ))
        sets.append((
            text: "Psssh", textColor: UIColor.yellow, min: 40, max: 40, pos: 0.5,
            shadow: (offset: CGPoint(x: -1, y: -2), color: UIColor.magenta, weight: 5, blur: 2),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: -0.15), CGPoint(x: 1, y: -0.3))
        ))
        sets.append((
            text: "shadow test Opx offset", textColor: UIColor.yellow, min: 20, max: 20, pos: 0.5,
            shadow: (offset: CGPoint(x: 0, y: 0), color: UIColor.magenta, weight: 10, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "shadow test Opx offset", textColor: UIColor.yellow, min: 50, max: 50, pos: 0.5,
            shadow: (offset: CGPoint(x: 0, y: 0), color: UIColor.magenta, weight: 10, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: "YoYoYoYoYoY", textColor: UIColor.yellow, min: 20, max: 40, pos: 0.5,
            shadow: (offset: CGPoint(x: 0, y: 0), color: UIColor.magenta, weight: 10, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0), CGPoint(x: 1, y: 0))
        ))
        sets.append((
            text: ".", textColor: UIColor.yellow, min: 20, max: 40, pos: 0.5,
            shadow: (offset: CGPoint(x: 0, y: 0), color: UIColor.magenta, weight: 10, blur: 0),
            curve: (CGPoint(x: 0, y: 0), CGPoint(x: 0.5, y: 0), CGPoint(x: 1, y: 0))
        ))
        
        applySettt(settt: sets[currentSet])
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.addSubview(label)
        label.frame.origin.y += 100
        //doubleTapSelectHandler.addTarget(self, action: #selector(selectTextEdit))
        //doubleTapSelectHandler.numberOfTapsRequired = 2
        //self.view.addGestureRecognizer(doubleTapSelectHandler)
        
        unfocusRecognizer.addTarget(self, action: #selector(unfocusTextEdit))
        self.view.addGestureRecognizer(unfocusRecognizer)
        
        self.view.addSubview(curveText)
        curveText.frame = self.view.frame
        //let scale = CGFloat(UIScreen.main.bounds.width / 300)
        curveText.frame.origin.y = 150
        curveText.frame.origin.x = 40
        //self.view.transform = CGAffineTransform.identity.scaledBy(x: scale, y: scale)
        
        let leftSlider = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
        leftSlider.direction = .left
        self.view.addGestureRecognizer(leftSlider)
        let rightSlider = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
        rightSlider.direction = .right
        self.view.addGestureRecognizer(rightSlider)
        
        let upSlider = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
        upSlider.direction = .up
        self.view.addGestureRecognizer(upSlider)
        let downSlider = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
        downSlider.direction = .down
        self.view.addGestureRecognizer(downSlider)
        
        //        for i in 0..<3 {
        //            let dotView = UIView()
        //            let dotShape = CAShapeLayer()
        //            let path = CGMutablePath()
        //            path.addArc(center: .zero, radius: 10, startAngle: 0, endAngle: CGFloat.pi * 2, clockwise: true)
        //            dotShape.path = path
        //            dotShape.fillColor = UIColor.blue.cgColor
        //            dotView.layer.addSublayer(dotShape)
        //            dotShape.frame.origin.x = 10
        //            dotShape.frame.origin.y = 10
        //            dotView.frame.size = CGSize(width: 20, height: 20)
        //            self.view.addSubview(dotView)
        //            dots.append(dotView)
        //        }
        //
        //        dots[0].frame.origin = curveText.curve.0.applying(CGAffineTransform(translationX: curveText.frame.minX, y: curveText.frame.minY))
        //        dots[1].frame.origin = curveText.curve.1.applying(CGAffineTransform(translationX: curveText.frame.minX, y: curveText.frame.minY))
        //        dots[2].frame.origin = curveText.curve.2.applying(CGAffineTransform(translationX: curveText.frame.minX, y: curveText.frame.minY))
    }
    
    @objc func swipe(e: UISwipeGestureRecognizer) {
        let editedFlag = sets[currentSet].text != curveText.text
        
        switch e.direction {
        case .left:
            currentSet = (currentSet + 1 + sets.count) % sets.count
        case .right:
            currentSet = (currentSet - 1 + sets.count) % sets.count
        case .up:
            currentFont = (currentFont - 1 + fonts.count) % fonts.count
        case .down:
            currentFont = (currentFont + 1 + fonts.count) % fonts.count
        default: break;
        }
        
        var set = sets[currentSet]
        
        if editedFlag {
            set.text = curveText.text
        }
        applySettt(settt: set)
        label.text = String(currentSet)
        label.sizeToFit()
    }
    
    @objc func unfocusTextEdit() {
        //        unfocusRecognizer.isEnabled = false
        //        doubleTapSelectHandler.isEnabled = true
        
        curveText.tf.resignFirstResponder()
    }
    
    
    /*var quadraticBezier = function(p1, p2, p3, t){
     return t*(t*(p3 - p2*2 + p1) + 2*(p2 - p1)) + p1;
     return (((p3 - p2)*t + p2) - ((p2 - p1)*t + p1))*t + ((p2 - p1)*t + p1);
     };*/
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
}
