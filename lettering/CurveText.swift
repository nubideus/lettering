//
//  CurveText.swift
//  lettering
//
//  Created by Eugenii Vlasenko on 02.10.17.
//  Copyright © 2017 Eugenii Vlasenko. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CurveText: UIView, UIGestureRecognizerDelegate, UITextViewDelegate {
    var tf = UITextView()
    private let shadowLayer = CALayer()
    private let textLayer = CALayer()
    private let defaultTextViewLeftInset = CGFloat(5)
    private let defaultTextViewRightInset = CGFloat(5)
    
    @IBInspectable var text: String = "CurveText" { didSet { setNeedsLayout() } }
    @IBInspectable var textColor: UIColor = UIColor.white { didSet { setNeedsDisplay() } }
    @IBInspectable var charVerticalAling: Int = -1 { didSet { setNeedsLayout() } }
    @IBInspectable var kernMultiplier: CGFloat = 0.05 { didSet { setNeedsDisplay() } }
    @IBInspectable var point1y: CGFloat { set { curvePattern.0.y = newValue }  get { return curvePattern.0.y }}
    @IBInspectable var point2: CGPoint { set { curvePattern.1 = newValue }  get { return curvePattern.1 }}
    @IBInspectable var point3y: CGFloat { set { curvePattern.2.y = newValue }  get { return curvePattern.2.y }}
    @IBInspectable var fontName: String = "" {
        didSet {
            guard fontName.count > 0, let newFont = UIFont(name: fontName, size: 1) else {
                self.font = UIFont.systemFont(ofSize: 1)
                return
            }
            self.font = newFont
        }
    }
    @IBInspectable var fontSizeMin: CGFloat = 20 { didSet { setNeedsLayout() } }
    @IBInspectable var fontSizeMax: CGFloat = 40 { didSet { setNeedsLayout() } }
    @IBInspectable var fontSizeMaxPosition: CGFloat = 0 { didSet { setNeedsLayout() } } // 0...100(%)
    @IBInspectable var shadowBlur: CGFloat = 0 { didSet { setNeedsDisplay() } }
    @IBInspectable var shadowWeight: CGFloat = 1 { didSet { setNeedsDisplay() } }
    @IBInspectable var shadowColor: UIColor = UIColor.black { didSet { setNeedsDisplay() } }
    @IBInspectable var shadowOffset: CGPoint = CGPoint(x: 0, y: 0) { didSet { setNeedsDisplay() } }
    @IBInspectable var isStairs: Bool = false { didSet { setNeedsLayout(); }}

    
    var curvePattern = (CGPoint(x: 0, y: 0), CGPoint(x: 50, y: 0), CGPoint(x: 100, y: 0)) {
        didSet {
            curve = curvePattern
            setNeedsLayout()
        }
    }
    var curve: (CGPoint, CGPoint, CGPoint)
    
    let minCharsForStartCalculating = 3
    var font: UIFont { didSet { setNeedsLayout() } }
    var scale: CGFloat = 1 { didSet { setNeedsLayout() } }
    
    //let font = UIFont(name: "COMIXON", size: 1)!
    //let font = UIFont(name: "LETTERING-Regular", size: 1)!
    //let font = UIFont.systemFont(ofSize: 1)
    
    private func applyFonts(_ attrStr: NSMutableAttributedString) {
        for index in 0..<attrStr.length {
            attrStr.addAttribute(.font, value: self.font.withSize(calcCharSize(attrStr, index: index) * scale), range: NSRange(location: index, length: 1))
            attrStr.addAttribute(.foregroundColor, value: UIColor.red.withAlphaComponent(0), range: NSRange(location: index, length: 1))
            attrStr.addAttribute(.kern, value: CGFloat(0), range: NSRange(location: index, length: 1))
        }
    }
    
    private func calcCharSize(_ attrStr: NSMutableAttributedString, index: Int) -> CGFloat {
        let base = CGFloat([attrStr.length, minCharsForStartCalculating].max()!) - 1
        
        var radius: CGFloat
        radius = [1 - fontSizeMaxPosition / 100, fontSizeMaxPosition / 100][base * (fontSizeMaxPosition / 100) - CGFloat(index) < 0 ? 0 : 1]
        //radius = [1 - maxPosition, maxPosition].max()!
        radius = [1, radius * base].max()!
        let koef = abs(base * fontSizeMaxPosition / 100 - CGFloat(index)) / radius
        let fontSize = CGFloat(fontSizeMax - fontSizeMin) * (1 - koef) + CGFloat(fontSizeMin)
        return fontSize + CGFloat(index) / 10000
    }
    
    private func applyGlyphsData(_ attrStr: NSMutableAttributedString) {
        for (index, char) in attrStr.string.enumerated() {
            let range = NSRange(location: index, length: 1)
            let font = attrStr.attribute(.font, at: index, effectiveRange: nil) as! UIFont
            var unichars = [UniChar](String(char).utf16)
            var glyphs = [CGGlyph](repeating: 0, count: unichars.count)
            
            let gotGlyphs = CTFontGetGlyphsForCharacters(font, &unichars, &glyphs, unichars.count)
            guard gotGlyphs else {
                continue
            }
            
            var boundingRects = [CGRect](repeating: .zero, count: unichars.count)
            var advances = [CGSize](repeating: .zero, count: unichars.count)
            
            let _ = CTFontGetBoundingRectsForGlyphs(font as CTFont, .horizontal, glyphs, &boundingRects, unichars.count)
            let _ = CTFontGetAdvancesForGlyphs(font as CTFont, .horizontal, glyphs, &advances, unichars.count)
            
            attrStr.addAttribute(NSAttributedStringKey("advance"), value: advances.first!.width, range: range)
            attrStr.addAttribute(NSAttributedStringKey("boundingRect"), value: boundingRects.first!, range: range)
            attrStr.addAttribute(NSAttributedStringKey("glyph"), value: glyphs.first!, range: range)
            
            for glyph in glyphs {
                var matrix = CGAffineTransform.identity
                if let path = CTFontCreatePathForGlyph(font as CTFont, glyph, &matrix) {
                    attrStr.addAttribute(NSAttributedStringKey(rawValue: "path"), value: path, range: range)
                }
            }
        }
    }
    
    private func applySerialAdvance(_ attrStr: NSMutableAttributedString) -> CGFloat {
        var advanceSum = CGFloat(0)
        for (index, _) in attrStr.string.enumerated() {
            let range = NSRange(location: index, length: 1)
            let advance = attrStr.attribute(NSAttributedStringKey("advance"), at: index, effectiveRange: nil) as! CGFloat
            let kern = attrStr.attribute(.kern, at: index, effectiveRange: nil) as? CGFloat ?? 0
            attrStr.addAttribute(NSAttributedStringKey("serialAdvance"), value: advanceSum, range: range)
            advanceSum += advance + kern
        }
        return advanceSum
    }
    
    private func applyKerning(_ attrStr: NSMutableAttributedString) {
        guard attrStr.string.count > 1 else {
            return
        }
        for index in 1..<attrStr.string.count {
            if let kern = getKernCorrection(attrStr, index) {
                
                attrStr.addAttribute(.kern, value: kern, range: NSRange(location: index - 1, length: 1))
            }
        }
    }
    
    private func getKernCorrection(_ attrStr: NSMutableAttributedString, _ index: Int) -> CGFloat? {
        let prevIndex = index - 1
        
        let advance = attrStr.attribute(NSAttributedStringKey("advance"), at: index, effectiveRange: nil) as! CGFloat
        let font = attrStr.attribute(.font, at: index, effectiveRange: nil) as! UIFont
        let serialAdvance = attrStr.attribute(NSAttributedStringKey("serialAdvance"), at: index, effectiveRange: nil) as! CGFloat
        let path: CGPath? = attrStr.attribute(NSAttributedStringKey("path"), at: index, effectiveRange: nil) as! CGPath?
        
        let prevAdvance = attrStr.attribute(NSAttributedStringKey("advance"), at: prevIndex, effectiveRange: nil) as! CGFloat
        let prevFont = attrStr.attribute(.font, at: prevIndex, effectiveRange: nil) as! UIFont
        let prevSerialAdvance = attrStr.attribute(NSAttributedStringKey("serialAdvance"), at: prevIndex, effectiveRange: nil) as! CGFloat
        let prevPath: CGPath? = attrStr.attribute(NSAttributedStringKey("path"), at: prevIndex, effectiveRange: nil) as! CGPath?
        
        if let prevPath = prevPath, let path = path {
            let pBaseline = -getBaselineOffset(attrStr, index)
            let prevPBaseline = -getBaselineOffset(attrStr, prevIndex)
            
            let point1 = CGPoint(x: prevSerialAdvance + prevAdvance / 2, y: prevPBaseline)
            let point2 = CGPoint(x: serialAdvance + advance / 2, y: pBaseline)
            
            let angle = -CGFloat(atan2(Double(point2.y - point1.y), Double(point2.x - point1.x)))
            
            var copyMatrix2 = CGAffineTransform.identity.rotated(by: angle).translatedBy(x: prevSerialAdvance, y: prevPBaseline).scaledBy(x: 1, y: -1)
            var copyMatrix = CGAffineTransform.identity.rotated(by: angle).translatedBy(x: serialAdvance, y: pBaseline).scaledBy(x: 1, y: -1)
            
            let dif = path.copy(using: &copyMatrix)!.boundingBoxOfPath.minX - prevPath.copy(using: &copyMatrix2)!.boundingBoxOfPath.maxX
            
            let minimumDistance = (prevFont.pointSize / 2 + font.pointSize / 2) * kernMultiplier
            
            var kern = -cos(angle) * (dif - minimumDistance)
            let dif2 = path.boundingBoxOfPath.offsetBy(dx: serialAdvance + kern, dy: 0).minX - prevPath.boundingBoxOfPath.offsetBy(dx: prevSerialAdvance, dy: 0).maxX
            
            if dif2 > minimumDistance {
                kern -= dif2 - minimumDistance
            }
            return kern
        }
        
        return nil
    }
    
    private func getBaselineOffset(_ attrStr: NSMutableAttributedString, _ index: Int) -> CGFloat {
        
        let charAttrs = attrStr.attributes(at: index, effectiveRange: nil)
        let position = (charAttrs[NSAttributedStringKey("serialAdvance")] as! CGFloat) + (charAttrs[NSAttributedStringKey("advance")] as! CGFloat) / 2
        let font = charAttrs[.font] as! UIFont
        
        if isStairs {
            //let index = index & 1 == 1 && index > 0 ? index - 1 : index
            let charAttrs = attrStr.attributes(at: index, effectiveRange: nil)
            let position = (charAttrs[NSAttributedStringKey("serialAdvance")] as! CGFloat) + (charAttrs[NSAttributedStringKey("advance")] as! CGFloat)
            
            var max: CGPoint = CGPoint(x: 0, y: -CGFloat.infinity)
            var tmax: CGFloat = 0
            for t in stride(from: CGFloat(0), to: CGFloat(1), by: CGFloat(0.05)) {
                let current = quadraticBezier(curve, t)
                if current.y > max.y {
                    max = current
                    tmax = t
                }
            }
            
            for i in (0..<index).reversed() {
                
            }
            if position < max.x {
                return -interpolate(curve.0, max, position / (max.x - curve.0.x)).y
            } else {
                return -interpolate(max, curve.2, (position - max.x) / (curve.2.x - max.x)).y
            }
        }
        
        
        return -(quadraticBezier(curve, rootsQuadraticBezier(curve, position).0).y + font.xHeight * CGFloat(charVerticalAling + 1) / 2)
    }
    
    private func applyBaseline(_ attrStr: NSMutableAttributedString) {
        for (index, _) in attrStr.string.enumerated() {
            let baselineOffset = getBaselineOffset(attrStr, index)
            attrStr.addAttribute(.baselineOffset, value: baselineOffset, range: NSRange(location: index, length: 1))
        }
    }
    
    private func calcGlobalBaselineOffset(_ attrStr: NSMutableAttributedString) -> CGFloat {
        var baselineGlobalOffset = CGFloat(0)
        
        for (index, _) in attrStr.string.enumerated() {
            let baselineOffset = attrStr.attribute(.baselineOffset, at: index, effectiveRange: nil) as! CGFloat
            let font = attrStr.attribute(.font, at: index, effectiveRange: nil) as! UIFont
            baselineGlobalOffset = [font.ascender + [baselineOffset, 0].max()!, baselineGlobalOffset].max()!
        }
        
        return baselineGlobalOffset
    }
    
    private func calcFrame(_ attrStr: NSMutableAttributedString) -> CGRect {
        var result: CGRect! = nil
        
        for (index, _) in attrStr.string.enumerated() {
            var boundingRect = attrStr.attribute(NSAttributedStringKey("boundingRect"), at: index, effectiveRange: nil) as! CGRect
            let kern = attrStr.attribute(.kern, at: index, effectiveRange: nil) as? CGFloat ?? 0
            let serialAdvance = attrStr.attribute(NSAttributedStringKey("serialAdvance"), at: index, effectiveRange: nil) as! CGFloat
            let baselineOffset = attrStr.attribute(.baselineOffset, at: index, effectiveRange: nil) as! CGFloat
            boundingRect = boundingRect.applying(CGAffineTransform.identity.scaledBy(x: 1, y: -1))
            boundingRect.size.width -= kern
            boundingRect.origin.x += serialAdvance
            boundingRect.origin.y -= baselineOffset
            result = (result ?? boundingRect).union(boundingRect)
        }
        
        return result ?? .zero
    }
    
    private func curveFit(newWidth: CGFloat) {
        let l = length(CGPoint(x: curve.0.x, y: 0), CGPoint(x: curve.2.x, y: 0))
        
        curve.2 = interpolate(curve.0, curve.2, newWidth / l)
        curve.1 = interpolate(curve.0, curve.1, newWidth / l)
    }
    
    private func updateVector(_ attrStr: NSMutableAttributedString) {
        
        let glyphsPath = CGMutablePath()
        for (index, _) in self.text.enumerated() {
            let baselineOffset = attrStr.attribute(.baselineOffset, at: index, effectiveRange: nil) as! CGFloat
            let serialAdvance = attrStr.attribute(NSAttributedStringKey("serialAdvance"), at: index, effectiveRange: nil) as! CGFloat
            
            let path = attrStr.attribute(NSAttributedStringKey("path"), at: index, effectiveRange: nil) as! CGPath?
            
            if let path = path {
                let matrix = CGAffineTransform.identity.translatedBy(x: serialAdvance, y: -baselineOffset).scaledBy(x: 1, y: -1)
                glyphsPath.addPath(path, transform: matrix)
            }
        }
        
        shadowLayer.sublayers?.forEach{ $0.removeFromSuperlayer() }
        if shadowWeight > 0 {
            let shadowSprite = CALayer()
            let strokeLayer = CAShapeLayer()
            strokeLayer.strokeColor = UIColor.black.cgColor
            strokeLayer.lineWidth = shadowWeight * 2 * scale
            strokeLayer.lineCap = kCALineCapRound
            strokeLayer.lineJoin = kCALineJoinRound
            strokeLayer.path = glyphsPath
            strokeLayer.lineDashPattern = [NSNumber(floatLiteral: Double.infinity), 0]
            shadowSprite.addSublayer(strokeLayer)
            
            let fillLayer = CAShapeLayer()
            fillLayer.fillColor = UIColor.black.cgColor
            fillLayer.path = glyphsPath
            shadowSprite.addSublayer(fillLayer)
            
            shadowSprite.shadowColor = shadowColor.cgColor
            let dropCoord = CGFloat(1000) * scale
            shadowSprite.shadowOffset = CGSize(width: self.shadowOffset.x * scale + dropCoord, height: self.shadowOffset.y * scale)
            shadowSprite.shadowRadius = self.shadowBlur * scale
            shadowSprite.shadowOpacity = 1
            shadowSprite.frame.origin.x -= dropCoord
            
            shadowLayer.addSublayer(shadowSprite)
        }
        
        let textShape = CAShapeLayer()
        textShape.fillColor = textColor.cgColor
        textShape.path = glyphsPath
        textLayer.sublayers?.forEach{ $0.removeFromSuperlayer() }
        textLayer.addSublayer(textShape)
        
        let lineShape = CAShapeLayer()
        lineShape.strokeColor = UIColor.black.cgColor
        lineShape.lineWidth = 1
        lineShape.fillColor = nil
        let linePath = CGMutablePath()
        linePath.move(to: curve.0)
        linePath.addQuadCurve(to: curve.2, control: curve.1)
        lineShape.path = linePath
        textLayer.addSublayer(lineShape)
        
        self.textLayer.removeAllAnimations()
        self.shadowLayer.removeAllAnimations()
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return self.frame.contains(point) || self.tf.frame.contains(point)
    }
    
    override func draw(_ rect: CGRect) {
        updateVector(NSMutableAttributedString(attributedString: tf.attributedText))
    }
    
    override func layoutSubviews() {
        let newString = self.text
        let attrStr = NSMutableAttributedString(string: newString)
        
        applyFonts(attrStr)
        applyGlyphsData(attrStr)
        
        var advancesSum: CGFloat = 0
        for i in 0...1 {
            advancesSum = applySerialAdvance(attrStr)
            
            for index in [newString.count, minCharsForStartCalculating].min()!..<minCharsForStartCalculating {
                advancesSum += calcCharSize(attrStr, index: index) * 0.6
            }
            curveFit(newWidth: advancesSum)
            if i == 0 {
                applyKerning(attrStr)
            }
        }
        applyBaseline(attrStr)
        
        let selection = tf.selectedTextRange
        tf.attributedText = attrStr
        tf.selectedTextRange = selection
        
        var calcedFrame = calcFrame(attrStr)
        
        if newString.count < minCharsForStartCalculating {
            if newString.count == 0 {
                calcedFrame = self.bounds
            }
            calcedFrame.size.width = advancesSum
        }
        
        tf.frame = calcedFrame
        tf.frame.origin.x = 0
        tf.frame.size.width = advancesSum
        self.frame.origin.y -= self.bounds.origin.y - calcedFrame.minY
        self.bounds = calcedFrame
        tf.frame = tf.frame.insetBy(dx: -(defaultTextViewLeftInset + defaultTextViewRightInset) / 2, dy: 0)
        setNeedsDisplay()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.text = textView.text
    }
    
    func initView() {
        self.frame.size.width = 0
        self.backgroundColor = .clear
        tf.delegate = self
        tf.backgroundColor = nil
        tf.textContainerInset = .zero
        tf.isScrollEnabled = false
        tf.scrollIndicatorInsets = .zero
        tf.showsVerticalScrollIndicator = false
        tf.showsHorizontalScrollIndicator = false
        tf.clipsToBounds = false
        self.addSubview(tf)
        self.layer.insertSublayer(shadowLayer, at: 0)
        self.layer.insertSublayer(textLayer, at: 1)
        
        self.layer.anchorPoint = CGPoint(x: 0, y: 0)
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
        
        var angle = CGFloat.pi / 2
        
        Timer.scheduledTimer(withTimeInterval: 1 / 60, repeats: true, block: {
            _ in
            for _ in 0..<2 {
                angle += 0.02
                
                self.curvePattern.1.y += cos(angle) / 100
            }
        })
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        curve = curvePattern
        font = UIFont.systemFont(ofSize: 1)
        super.init(frame: frame)
        initView()
    }
    required init?(coder aDecoder: NSCoder) {
        curve = curvePattern
        font = UIFont.systemFont(ofSize: 1)
        super.init(coder: aDecoder)
        initView()
    }
    
    override func prepareForInterfaceBuilder() {
        initView()
    }
}

func pointAtBrokenLine(_ points: [CGPoint], position: CGFloat) -> (CGPoint, CGPoint)? {
    var sumLength: CGFloat = 0
    
    guard points.count > 1 else {
        return nil
    }
    guard position > 0 else {
        return (points[0], halfVec(points[1], points[0], points[1]))
    }
    
    var prevPoint: CGPoint = points.first!
    
    for i in 1..<points.count {
        let point = points[i]
        let len = length(prevPoint, point)
        
        if sumLength + len == position || i == points.count - 1 && (position - sumLength - len < CGFloat.ulpOfOne * CGFloat(points.count)) {
            let nextPoint = i < points.count - 1 ? points[i + 1] : prevPoint
            
            if (nextPoint != prevPoint) {
                return (point, CGPoint(x: (point.x - prevPoint.x) / len, y: (point.y - prevPoint.y) / len))
            }
            
            return (point, halfVec(prevPoint, point, nextPoint))
        }
        
        if sumLength + len > position {
            return (interpolate(prevPoint, point, (position - sumLength) / len), CGPoint(x: -(prevPoint.y - point.y) / len, y: (prevPoint.x - point.x) / len ))
        }
        
        sumLength += len
        prevPoint = point
    }
    
    return nil
}

func halfVec(_ p1: CGPoint, _ p2: CGPoint, _ p3: CGPoint) -> CGPoint {
    var halfPoint = CGPoint(
        x: (p1.x - p2.x) + (p1.x - p3.x),
        y: (p1.y - p2.y) + (p1.y - p3.y)
    )
    
    if skewProduct(sub(p1, p2), sub(p2, p3)) >= 0 {
        halfPoint.x *= -1
        halfPoint.y *= -1
    }
    let len = length(halfPoint, p2)
    halfPoint.x /= len
    halfPoint.y /= len
    return halfPoint
}

func quadraticBezierToBrokenLine(_ points: (CGPoint, CGPoint, CGPoint), quality: CGFloat = 1) -> [CGPoint] {
    guard CGFloat.ulpOfOne...1 ~= quality else {
        return [points.0, points.1, points.2]
    }
    let step = (1 / quality) / (length(points.0, points.1) + length(points.1, points.2))
    
    return stride(from: 0, to: 1, by: step).map{
        return quadraticBezier(points, $0)
        } + [points.2]
}

func rootsQuadraticBezier(_ points: (CGPoint, CGPoint, CGPoint), _ x: CGFloat) -> (CGFloat, CGFloat) {
    let a = [points.2.x - points.1.x * 2 + points.0.x, 1].max()!
    let b = 2 * (points.1.x - points.0.x)
    let c = points.0.x - x
    
    let d = b * b - 4 * a * c
    
    return (
        (-b + sqrt(d)) / (2 * a),
        (-b - sqrt(d)) / (2 * a)
    )
}

func rootLine(_ points: (CGPoint, CGPoint), _ x: CGFloat) -> CGFloat {
    return (x - points.0.x) / (points.1.x - points.0.x)
}

func quadraticBezier(_ points: (CGPoint, CGPoint, CGPoint), _ t: CGFloat) -> CGPoint {
    return CGPoint(x: quadraticBezier((points.0.x, points.1.x, points.2.x), t), y: quadraticBezier((points.0.y, points.1.y, points.2.y), t))
}

func quadraticBezier(_ points: (CGFloat, CGFloat, CGFloat), _ t: CGFloat) -> CGFloat {
    return points.0 + t * (t * (points.2 - points.1 * 2 + points.0) + 2 * (points.1 - points.0));
}

func skewProduct(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat {
    return p1.x * p2.y - p2.x * p1.y
}

func sub(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint {
    return CGPoint(x: p1.x - p2.x, y: p1.y - p2.y)
}

func add(_ p1: CGPoint, _ p2: CGPoint) -> CGPoint {
    return CGPoint(x: p1.x + p2.x, y: p1.y + p2.y)
}

func length(_ points: [CGPoint]) -> CGFloat? {
    guard points.count > 1 else { return nil }
    return points.reduce((points.first!, CGFloat(0)), {
        return ($1, $0.1 + length($0.0, $1))
    }).1
}

func length(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat {
    return length(CGPoint(x: p2.x - p1.x, y: p2.y - p1.y))
}

func length(_ p: CGPoint) -> CGFloat {
    return (p.x * p.x + p.y * p.y).squareRoot()
}

func interpolate(_ p1: CGPoint, _ p2: CGPoint, _ t: CGFloat) -> CGPoint {
    return CGPoint(x: (p2.x - p1.x) * t + p1.x, y: (p2.y - p1.y) * t + p1.y)
}
